var path = require('path');
var fs = require('fs');
var moment = require('moment');
var request = require('request');
var mkdirp = require('mkdirp');
var async = require('async');
var colog = require('colog');

var stdio = require('stdio');
var options = stdio.getopt({
    'site': { key: 's', mandatory: true, args: 1, description: 'URL of Wialon Site (i.e. https://hst-api.wialon.com)' },
    'token': { key: 'k', args: 1, description: 'Token of Wialon user' },
    'from': { key: 'f', mandatory: false, args: 1, description: 'From date/time in "YYYY-MM-DD HH:mm:ss" format (365 days back by default)' },
    'to':   { key: 't', mandatory: false, args: 1, description: 'To date/time (exclusive) in "YYYY-MM-DD HH:mm:ss" format (now by default)' },
    'format': { args: 1, default: 'wlb', description: 'Format (wlb|wln|plt|txt|kml) of Wialon files ' },
    'folder': { args: 1, default: '', description: 'Output Folder for Wialon files' },
    'nocompression': { description: 'No Compression of Wialon files' },
    'nomsgsok': { description: 'No Messages is OK' },
    'user': { key: 'u', args: 1, description: 'Name of Wialon User (legacy login method) ' },
    'pass': { key: 'p', args: 1, description: 'Password of Wialon user (legacy login method) ' },
});

if (options.to) {
    var to = moment(options.to, 'YYYY-MM-DD HH:mm:ss');
    if (!to.isValid()) {
        colog.error("Invalid 'To' date/time (--to argument). Expected \"YYYY-MM-DD HH::mm:ss\" format.");
        return;
    }
    options.to = to.unix();
} else {
    options.to = moment().unix();
}
if (options.from) {
    var from = moment(options.from, 'YYYY-MM-DD HH:mm:ss');
    if (!from.isValid()) {
        colog.error("Invalid 'From' date/time (--from argument). Expected \"YYYY-MM-DD HH:mm:ss\" format.");
        return;
    }
    options.from = from.unix();
} else {
    options.from = options.to - 3600*24*365;
}
colog.info('Date range: [' + moment.unix(options.from).format('YYYY-MM-DD HH:mm:ss') +
                      ', ' + moment.unix(options.to).format('YYYY-MM-DD HH:mm:ss') + ').');
if (options.from >= options.to) {
    colog.error("'From' date/time should be less than 'To' date/time.");
    return;
}

var qooxdoo = require("qooxdoo");
var wialon_sdk = require("wialon_sdk");

var session = wialon.core.Session.getInstance();
session.initSession(options.site);
var is_logged_in = false;

var remote = wialon.core.Remote.getInstance();
remote.replaceSender("sdk", new wialon.core.NodeHttp(remote.createFullUrl(session.getBaseUrl()) + "/wialon/post.html", 0));
remote.replaceSender("search", new wialon.core.NodeHttp(remote.createFullUrl(session.getBaseGisUrl("search")) + "/gis_post?1", 1));
remote.replaceSender("geocode", new wialon.core.NodeHttp(remote.createFullUrl(session.getBaseGisUrl("geocode")) + "/gis_post?2", 2));

var stat = { oks:0, warns:0, errors:0 };

async.series(
    [
        login,
        init_data_flags,
        enum_units
    ],
    cleanup
);

function cleanup(err) {
    async.series(
        [
            logout
        ],
        function() {
            if (err) {
                colog.headerError("Finished with " + err).nl();
            } else {
                if (stat.errors) {
                    colog.headerError("Finished with errors.");
                } else if (stat.warns) {
                    colog.headerWarning("Finished with warnings.");
                } else {
                    colog.headerSuccess("Finished OK.");
                }
                colog.nl();
            }
            process.exit(err ? 1 : 0);
        }
    );
}

function login(return_cb) {
    colog.info("Loggin in... ");
    if (options.token) {
        session.loginToken(options.token, "", function (err) {
            if (err) {
                colog.error('Failed to login: ' + wialon.core.Errors.getErrorText(err));
            } else {
                is_logged_in = true;
            }
            return_cb(err);
        });
    } else {
        session.login(options.user, options.pass, "", function (err) {
            if (err) {
                colog.error('Failed to login: ' + wialon.core.Errors.getErrorText(err));
            } else {
                is_logged_in = true;
            }
            return_cb(err);
        });
    }
}

function logout(return_cb) {
    if (is_logged_in) {
        colog.info("Logging out... ");
        session.logout(function (err) {
            // discard any logout errors
            is_logged_in = false;
            return_cb();
        });
    } else {
        return_cb();
    }
}

function init_data_flags(return_cb) {
    colog.info("Updating data flags... ");
    session.updateDataFlags(
        [{
            type: "type",
            data: "avl_unit",
            flags: wialon.item.Item.dataFlag.base,
            mode: 0
        }],
        function (err) {
            if (err) {
                colog.error('Failed to update flags: ' + wialon.core.Errors.getErrorText(err));
            }
            return_cb(err);
        }
    );
}

function enum_units(return_cb) {
    var units = session.getItems('avl_unit');
    colog.info('Found ' + units.length + ' units. Enumerating...');

    var i = 0;
    async.eachSeries(units,
        function(unit, next_unit_cb) {

            ++i;
            var id = unit.getId();
            var name = unit.getName();
            colog.info(i + ')\t' + name + ' (id=' + id + ')');
            var folder = path.normalize(options.folder);
            var file_name = path.join(folder, name + '.' + options.format + (options.nocompression ? '' : '.zip'));

            async.waterfall([
                function(next_task_cb) {
                    mkdirp(folder, function(err, made) {
                        next_task_cb(err);
                    });
                },
                function(next_task_cb) {
                    create_unit_messages_layer(id, options.to, options.from, function(err, layer) {
                        next_task_cb(err, layer);
                    });
                },
                function(layer, next_task_cb) {
                    if (!layer) {
                        if (options.nomsgsok) {
                            stat.oks++;
                            colog.log('\t\t0 messages.')
                        } else {
                            stat.warns++;
                        }
                        next_task_cb();
                        return;
                    }
                    download_unit_messages_layer(id, file_name, function() {
                        stat.oks++;
                        colog.log('\t\t' + layer._data.units[0].msgs.count + ' messages.')
                        next_task_cb();
                    });
                }
            ], function(err) {
                if (err) stat.errors++;
                next_unit_cb();
            });
        },
        function () {
            colog.info('Enumeration completed: ok='+stat.oks+', warnings='+stat.warns+', errors='+stat.errors+'.');
            return_cb();
        }
    );
}

function create_unit_messages_layer(id, to, from, return_cb) {
    params = {
        "layerName": "messages_" + id,
        "itemId": id,
        "timeFrom": from,
        "timeTo": to,
        "tripDetector": 0,
        "trackColor": "speed",
        "trackWidth": 4,
        "arrows": 0,
        "points": 0,
        "pointColor": "cc0000ff",
        "annotations": 0
    };
    session.getRenderer().createMessagesLayer(params, function(err, layer) {
        if (err) {
            if (err == 1001) {
                if (!options.nomsgsok) {
                    colog.warning('\t\t' + wialon.core.Errors.getErrorText(err));
                }
                return_cb(null, null);
            } else {
                colog.error('\t\t' + 'Failed to create messages layer. ' + wialon.core.Errors.getErrorText(err));
                return_cb(err);
            }
        } else {
            return_cb(null, layer);
        }
    });
}

function download_unit_messages_layer(id, file_name, return_cb) {
    var layer_name = "messages_" + id;
    var export_url = wialon.exchange.Exchange.getMessagesExportUrl(layer_name, options.format, !options.nocompression);
    download_file(export_url, file_name, function() {
        return_cb();
    });
}

function download_file(url, dest, return_cb) {
    request(url).on('end', return_cb).pipe(fs.createWriteStream(dest));
}
